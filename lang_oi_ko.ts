<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko_KR">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="21"/>
        <source>프로브 서버  가동</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="28"/>
        <source>count </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="35"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="113"/>
        <location filename="mainwindow.cpp" line="152"/>
        <source>RUN</source>
        <translation>정상가동</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="115"/>
        <source>ENGR1</source>
        <translation>공정,설비 테스트</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="117"/>
        <source>SCHDOWN1</source>
        <translation>예방정비</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="119"/>
        <source>USCHDOWN3</source>
        <translation>설비고장</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="121"/>
        <source>WAIT</source>
        <translation>대기</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="123"/>
        <source>NONSCHED</source>
        <translation>휴지</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="125"/>
        <source>SCHDOWN2</source>
        <translation>기종변경</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="127"/>
        <source>USCHDOWN1</source>
        <translation>자재품절</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="129"/>
        <source>USCHDOWN2</source>
        <translation>품질문제</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="131"/>
        <source>USCHDOWN4</source>
        <translation>순간정지</translation>
    </message>
</context>
</TS>
