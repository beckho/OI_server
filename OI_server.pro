#-------------------------------------------------
#
# Project created by QtCreator 2017-06-08T14:03:14
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OI_server
TEMPLATE = app

TRANSLATIONS += lang_oi_ko.ts

SOURCES += main.cpp\
        mainwindow.cpp \
    oisystemsoap/soapC.cpp \
    oisystemsoap/soapOIWebServiceSoapProxy.cpp \
    oisystemsoap/stdsoap2.cpp

HEADERS  += mainwindow.h \
    oisystemsoap/oisystem.h \
    oisystemsoap/soapH.h \
    oisystemsoap/soapOIWebServiceSoapProxy.h \
    oisystemsoap/soapStub.h \
    oisystemsoap/stdsoap2.h \
    oisystemsoap/OIWebServiceSoap.nsmap

FORMS    += mainwindow.ui

RESOURCES += \
    res.qrc
